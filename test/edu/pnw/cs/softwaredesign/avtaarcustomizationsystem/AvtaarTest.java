package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AvtaarTest {

  @Test
  public void test_avtaar() {
    
    Avtaar avtaar = new Male();
    String desc = avtaar.getDescription();
    String descResult = "Male";
    assertEquals("The test is not passed", descResult, desc);
  }
  
  @Test
  public void test_cosmetic() {
    
    Avtaar avtaar = new Male();
    avtaar = new Tshirt(avtaar);
    avtaar = new Highheels(avtaar);
    String desc = avtaar.getCosmeticDescription();
    System.out.println(avtaar.getCosmeticDescription());
    String descResult = ", High Heels , T-shirt ";
    assertEquals("The test is not passed", descResult, desc);
  }

}
