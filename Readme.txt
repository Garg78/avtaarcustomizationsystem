A Avtaar Customization System where you select an avtaar's gender and 
add the cosmetics based on the input from the user.

Sample UI Example :

Welcome to the Avatar 1.0 System!
Please select your character:
1.	Male 
2.	Female
>> 1
You have selected the Male.

Please select a cosmetic for your character:
(Type �exit� to finish)

1.	Jacket
2.	T-shirt
3.	Sweater
4.	Jeans
5.	Shorts
6.	Tie
7.	Neckless
8.	Earrings
9.	Sunglasses
10.	Leather Shoes
11.	High Heels
12.	Running Shoes

>> 6
You have selected Tie for your character.

Please select a cosmetic for your character:
(Type �exit� to finish)

1.	Jacket
2.	T-shirt
3.	Sweater
4.	Jeans
5.	Shorts
6.	Tie
7.	Neckless
8.	Earrings
9.	Sunglasses
10.	Leather Shoes
11.	High Heels
12.	Running Shoes

>> 12
You have selected Running Shoes for your character.

Please select a cosmetic for your character:
(Type �exit� to finish)

1.	Jacket
2.	T-shirt
3.	Sweater
4.	Jeans
5.	Shorts
6.	Tie
7.	Neckless
8.	Earrings
9.	Sunglasses
10.	Leather Shoes
11.	High Heels
12.	Running Shoes

>>exit
Your character has the following items :
Tie, Running Shoes
