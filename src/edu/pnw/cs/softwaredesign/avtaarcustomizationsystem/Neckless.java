package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Neckless extends Cosmetic {

  public Neckless(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Neckless " + getAvtaar().getCosmeticDescription();
  }
}
