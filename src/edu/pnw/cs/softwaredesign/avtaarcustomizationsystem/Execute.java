package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

import java.util.Scanner;

/**
 * The main method for AvtaarCustomization Software based on Decorator Pattern.
 * @author Arushee Garg
 * @version 1.0
 */
public class Execute {

  /**
   * The main method based on command line user input of gender and cosmetics to
   * be selected for the avtaar.
   */
  public static void main(String[] args) {

    AvtaarCustomization acsystem = new AvtaarCustomization();

    System.out.println("Welcome to the Avatar 1.0 System! \n" 
        + "Please select your character: \n1.Male \n2.Female");
    Scanner scanner = new Scanner(System.in);
    int gender = scanner.nextInt();

    Avtaar avtaar = acsystem.genderInput(gender);

    String cosmetic;
    do {
      System.out.println("Please select a cosmetic for your character: \n"
          + "(Type �exit� to finish)\n");
      System.out.println("1.Jacket \n2.T-shirt \n3.Sweater \n4.Jeans \n5.Shorts \n6.Tie \n"
          + "7.Neckless \n8.Earrings \n9.Sunglasses \n" + "10."
              + "Leather Shoes \n11.High Heels \n12.Running Shoes");
      cosmetic = scanner.next();
      if (!cosmetic.equals("exit")) {
        int i = Integer.parseInt(cosmetic);
        avtaar = acsystem.cosmeticInput(avtaar, i);
      }
    } while (!cosmetic.equals("exit"));

    scanner.close();
    System.out.println("Your Character has the following items :");
    System.out.println(avtaar.getCosmeticDescription());

  }

}
