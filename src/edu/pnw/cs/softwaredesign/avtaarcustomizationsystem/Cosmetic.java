package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

/**
 * The abstarct Cosmetic Class which extends Avtaar class for the guidelines 
 * of the cosmetics to be added to the avtaar based on the Decorator Pattern.
 *  @author Arushee Garg
 *  @version 1.0
 */
public abstract class Cosmetic extends Avtaar {

  private Avtaar avtaar;

  public Avtaar getAvtaar() {
    return this.avtaar;
  }

  public void setAvtaar(Avtaar avtaar) {
    this.avtaar = avtaar;
  }
  /**
   * getCosmeticDescription method returns the description of all the
   * cosmetics which the avtaar contains.
   */
  
  public abstract String getCosmeticDescription();
}
