package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Leathershoes extends Cosmetic {

  public Leathershoes(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Leathershoes " + getAvtaar().getCosmeticDescription();
  }
}
