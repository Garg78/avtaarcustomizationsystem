package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Shorts extends Cosmetic {
  
  
  public Shorts(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Shorts " + getAvtaar().getCosmeticDescription();
  }

}
