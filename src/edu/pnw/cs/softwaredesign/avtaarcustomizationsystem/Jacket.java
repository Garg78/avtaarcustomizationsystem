package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Jacket extends Cosmetic {

  public Jacket(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }

  @Override
  public String getCosmeticDescription() {
    return ", Jacket " + getAvtaar().getCosmeticDescription();
  }

}
