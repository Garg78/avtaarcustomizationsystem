package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Highheels extends Cosmetic {

  public Highheels(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }

  @Override
  public String getCosmeticDescription() {
    return ", High Heels " + getAvtaar().getCosmeticDescription();
  }

}
