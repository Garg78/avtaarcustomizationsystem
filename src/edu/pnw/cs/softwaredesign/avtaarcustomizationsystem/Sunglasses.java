package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Sunglasses extends Cosmetic {

  
  public Sunglasses(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Sunglasses " + getAvtaar().getCosmeticDescription();
  }

}
