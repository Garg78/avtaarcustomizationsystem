package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

/**
 * The abstarct Avtaar Class for the guidelines of the avtaar.
 *  @author Arushee Garg
 *  @version 1.0
 */
public abstract class Avtaar {

  private String description;

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public String getCosmeticDescription() {
    return "";
  }

}
