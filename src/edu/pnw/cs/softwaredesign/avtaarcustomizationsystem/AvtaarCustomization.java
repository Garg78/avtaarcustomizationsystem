package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;
/**
 * AvtaarCustomization class is a supporter class for the execute 
 * method to take gender and cosmetic input from the user.
 * @author Arushee Garg
 * @version 1.0
 */

public class AvtaarCustomization {

  /**
   * genderInput method to create the avtaar based on the gender mentioned.
   * @param gender : user input from the user
   * @return : avtaar
   */
  public Avtaar genderInput(int gender) {
    Avtaar avtaar;
    if (gender == 1) {
      System.out.println("You have selected a Male Avtaar");
      avtaar = new Male();
    } else if (gender == 2) {
      System.out.println("You have selected a Female Avtaar");
      avtaar = new Female();
    } else {
      throw new IllegalArgumentException("invalid input please select either 1 or 2");
    }
    return avtaar;
  }

  /**
   * cosmeticInput method to add the cosmetic to the avtaar.
   * @param avtaar : user input from the user
   * @param cosmetic : user input from the user
   * @return avtaar
   */
  public Avtaar cosmeticInput(Avtaar avtaar, int cosmetic) {

    switch (cosmetic) {
      case 1:
        System.out.println("You have selected Jacket for your character\n");
        avtaar = new Jacket(avtaar);
        break;
      case 2:
        System.out.println("You have selected Tshirt for your character\n");
        avtaar = new Tshirt(avtaar);
        break;
      case 3:
        System.out.println("You have selected Sweater for your character\n");
        avtaar = new Sweater(avtaar);
        break;
      case 4:
        System.out.println("You have selected Jeans for your character\n");
        avtaar = new Jeans(avtaar);
        break;
      case 5:
        System.out.println("You have selected Shorts for your character\n");
        avtaar = new Shorts(avtaar);
        break;
      case 6:
        System.out.println("You have selected Tie for your character\n");
        avtaar = new Tie(avtaar);
        break;
      case 7:
        System.out.println("You have selected Neckless for your character\n");
        avtaar = new Neckless(avtaar);
        break;
      case 8:
        System.out.println("You have selected Earrings for your character\n");
        avtaar = new Earrings(avtaar);
        break;
      case 9:
        System.out.println("You have selected Sunglasses for your character\n");
        avtaar = new Sunglasses(avtaar);
        break;
      case 10:
        System.out.println("You have selected Leathershoes for your character\n");
        avtaar = new Leathershoes(avtaar);
        break;
      case 11:
        System.out.println("You have selected Highheels for your character\n");
        avtaar = new Highheels(avtaar);
        break;
      case 12:
        System.out.println("You have selected Runningshoes for your character\n");
        avtaar = new Runningshoes(avtaar);
        break;
      default:
        System.out.println("Wrong selection. ");
    }
    return avtaar;
  }
}
