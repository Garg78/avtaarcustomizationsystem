package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Tie extends Cosmetic {

  public Tie(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Tie " + getAvtaar().getCosmeticDescription();
  }

}
