package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Runningshoes extends Cosmetic {
  
  public Runningshoes(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Running Shoes " + getAvtaar().getCosmeticDescription();
  }

}
