package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Sweater extends Cosmetic {

  public Sweater(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", Sweater " + getAvtaar().getCosmeticDescription();
  }

}
