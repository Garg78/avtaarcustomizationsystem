package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Jeans extends Cosmetic {

  public Jeans(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }

  @Override
  public String getCosmeticDescription() {
    return ", Jeans " + getAvtaar().getCosmeticDescription();
  }
}
