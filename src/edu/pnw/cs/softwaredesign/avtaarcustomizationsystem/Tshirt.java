package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Tshirt extends Cosmetic {
  public Tshirt(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }
  
  @Override
  public String getCosmeticDescription() {
    return ", T-shirt " + getAvtaar().getCosmeticDescription();
  }


}
