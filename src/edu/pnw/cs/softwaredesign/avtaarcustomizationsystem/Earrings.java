package edu.pnw.cs.softwaredesign.avtaarcustomizationsystem;

public class Earrings extends Cosmetic {

  public Earrings(Avtaar avtaar) {
    super();
    setAvtaar(avtaar);
  }

  @Override
  public String getCosmeticDescription() {
    return ", Earrings " + getAvtaar().getCosmeticDescription();
  }
}
